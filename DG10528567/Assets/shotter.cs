﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shotter : MonoBehaviour {
    private float nextfire = 0;
    public GameObject bullet;
    public float rotespeed = 1;
	// Use this for initialization
	void Start () {
        nextfire = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Space) && Time.time > nextfire) {
            Debug.Log("1");
            nextfire = Time.time + 1;
            Instantiate(bullet, transform.position, transform.rotation);
        }
        if (Input.GetKey(KeyCode.RightArrow)) {
            transform.Rotate(new Vector3(0, rotespeed, 0));
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(new Vector3(0, -rotespeed, 0));
        }
	}
}
