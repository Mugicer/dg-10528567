﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour {
    public float speed = 1;
    Rigidbody rigidbody;
	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = transform.forward * speed ;
	}
	
	// Update is called once per frame
	void Update () {
        
	}
    void OnCollisionEnter(Collision c) {
        if (c.gameObject.tag == "score") {
            c.gameObject.SetActive(false);
            Destroy(this.gameObject);
        }
        Destroy(this.gameObject, 10);
    }
}
